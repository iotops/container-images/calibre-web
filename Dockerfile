FROM  ubuntu:18.04 AS base 

RUN apt-get update && apt-get install -y \
  imagemagick \
  libxcomposite1 \ 
  python-minimal 

FROM base AS build

RUN apt-get install -y \
  git python-pip \
  unzip \
  curl \
  python-dev \
  libsasl2-dev \
  libldap2-dev \
  libssl-dev

WORKDIR /app

RUN curl -L https://github.com/janeczku/calibre-web/archive/master.zip > /app/master.zip \
    && cd /app && unzip master.zip && rm master.zip 

WORKDIR /app/calibre-web-master/

RUN pip install --no-cache-dir --target vendor -r requirements.txt -r optional-requirements.txt 

FROM base AS final
RUN rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
WORKDIR /app

COPY --from=build /app/calibre-web-master  /app

CMD [ "python", "cps.py" ] 
